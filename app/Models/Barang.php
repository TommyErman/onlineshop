<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{

    protected $fillable = ['nama_barang', 'harga', 'stok', 'gambar', 'keterangan'];
    protected $hidden = ['create_at', 'update_at'];
    public function pesanan_detail()
    {
        //relasi dari barang ke pesanan_detail
        return $this->hasMany(PesananDetail::class, 'barang_id', 'id');
    }
}
