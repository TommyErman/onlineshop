<?php

use App\Models\Pesanan;
use App\Models\PesananDetail;
use Illuminate\Support\Facades\Auth;
?>



<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
    integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css2?family=Quicksand&family=Viga&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="{{asset('style')}}/plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="{{asset('style')}}/assets/css/style.css">
  <title>ESTHETIC</title>
</head>

<body class="profile">
  <!-- navbar -->

  <nav class="navbar navbar-expand-lg navbar-light">
    <div class="container">
      <a class="navbar-brand" href="#">ESTHETIC</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
        aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav ml-auto">
          <a class="nav-item nav-link active" href="{{route('home')}}">Profile<span class="sr-only">(current)</span></a>
          <a class="nav-item nav-link" href="{{url('pesan')}}">Belanja</a>
          <a class="nav-item nav-link" href="kategori.html">Ketegori</a>
          <a class="nav-item nav-link" href="#">Transaksi</a>
          <li>
            <?php
            $pesanan_utama = Pesanan::where('user_id', Auth::user()->id)->where('status',0)->first();
            if(!empty($pesanan_utama))
               {
                $notif =PesananDetail::where('pesanan_id', $pesanan_utama->id)->count(); 
               }
           ?>


            <a class="nav-item nav-link" href="{{url('checkout')}}">
              <i class="fa fa-shopping-cart"></i>
              @if (!empty($notif))
                  
              
                  <span class="badge badge-danger">{{$notif}}</span>
                  
              @endif
            
            
            </a>

          </li>

          <li class="nav-item dropdown">
            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                {{ Auth::user()->name }} <span class="caret"></span>
            </a>

            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="{{ url('profile') }}">
                    Profile
                </a>

                <a class="dropdown-item" href="{{ url('history') }}">
                    Riwayat Pemesanan
                </a>

                <a class="dropdown-item" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </li>


        
        </div>
      </div>
    </div>
  </nav>
  <!-- AKHIR NAV -->

  @if (session('status'))
  <div class="alert alert-success">
      {{ session('status') }}
  </div>
  @endif

  
  <!-- partner -->
  <section class="partner" id="partner">
    <div class="row">
      <div class="col-12">
        <h3 class="text-center">Partner</h3>
      </div>
    </div>
    <div class="container">
      <div class="row client ">
        <div class="col-lg ">
          <img src="{{asset('style')}}/assets/img/zara.png" alt="" class="img-fluid">
        </div>
        <div class="col-lg">
          <img src="{{asset('style')}}/assets/img/ck2.png" alt="" class="img-fluid">
        </div>
        <div class="col-lg">
          <img src="{{asset('style')}}/assets/img/hugo.jpg" alt="" class="img-fluid">
        </div>
        <div class="col-lg">
          <img src="{{asset('style')}}/assets/img/aigner.png" alt="" class="img-fluid">
        </div>
      </div>
    </div>
  </section>
  <!-- akhir patner -->

  <section class="kategori" id="kategori">
    <div class="container">
      <div class="row">
        <div class="col-12 text-center">
          <h2>Produk</h2>
        </div>
      </div>
      
        <div class="row">
          @foreach ($barangs as $barang)
          <div class="col-md-4">
            <div class="card">
              <img src="{{ asset('images') }}/{{ $barang->gambar }}" class="card-img-top" alt="...">
              <div class="card-body">
                <h5 class="card-title">{{ $barang->nama_barang }}</h5>
                <p class="card-text">
                    <strong>Harga :</strong> Rp. {{ number_format($barang->harga)}} <br>
                    <strong>Stok :</strong> {{ $barang->stok }} <br>
                    <hr>
                    <strong>Keterangan :</strong> <br>
                    {{ $barang->keterangan }} 
                </p>
                <a href="{{ url('/pesan/detail') }}/{{ $barang->id }}" class="btn btn-primary"><i class="fa fa-shopping-cart"></i> Pesan</a>
              </div>
            </div> 
        </div>
            @endforeach
       </div>
      
    </div>
  </section>



  



  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
    integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
    crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
    integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
    crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
    integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
    crossorigin="anonymous"></script>
    {{-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> --}}
</body>

</html>