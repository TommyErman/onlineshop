<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use Illuminate\Http\Request;

class BarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $barangs = Barang::all();
        return view('barang.index', compact('barangs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('barang/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'nama_barang' => 'required',
            'harga' => 'required',
            'stok' => 'required',
            'gambar' => 'required',
            'keterangan' => 'required',
        ], [
            'nama_barang.required' => 'Nama Barang Harus di Isi',
            'harga.required' => 'Harga Harus di Isi',
            'stok.required' => 'Stok Harus di Isi',
            'gambar.required' => 'Gambar Harus di Isi',
            'keterangan.required' => 'keterangan telepon Harus di Isi',

        ]);

        $barang = Barang::create($request->all());
        if ($request->hasFile('gambar')) {
            $request->file('gambar')->move('images/', $request->file('gambar')->getClientOriginalName());
            $barang->gambar = $request->file('gambar')->getClientOriginalName();
            $barang->save();
        }
        return redirect('barang')->with('status', 'Data Berhasil Ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Barang $barang)
    {
        return view('barang/show', compact('barang'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Barang $barang)
    {
        return view('barang/edit', compact('barang'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Barang $barang)
    {

        $request->validate([
            'nama_barang' => 'required',
            'harga' => 'required',
            'stok' => 'required',
            'gambar' => 'required',
            'keterangan' => 'required',
        ], [
            'nama_barang.required' => 'Nama Barang Harus di Isi',
            'harga.required' => 'Harga Harus di Isi',
            'stok.required' => 'Stok Harus di Isi',
            'gambar.required' => 'Gambar Harus di Isi',
            'keterangan.required' => 'keterangan telepon Harus di Isi',

        ]);


        $barang = Barang::where('id', $barang->id)
            ->update(
                [
                    'nama_barang' => $request->nama_barang,
                    'harga' => $request->harga,
                    'stok' => $request->stok,
                    'gambar' => $request->gambar,
                    'keterangan' => $request->keterangan

                ]

            );


        return redirect('barang')->with('status', 'data berhasil di Update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Barang $barang)
    {
        Barang::destroy($barang->id);
        return redirect('barang')->with('status', 'data berhasil di hapus!!');
    }
}
