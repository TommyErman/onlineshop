<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use App\Models\Konfirmasi_pembayaran;
use App\Models\Pesanan;
use Illuminate\Http\Request;

class KonfirmasiController extends Controller
{
    public function index()
    {
        // $pesanan = Pesanan::where('id', $id)->first();

        // // $konfirmasi = Konfirmasi_pembayaran::where('pesanan_id', $barang->id);

        // $konfirmasi = new Konfirmasi_pembayaran;
        // $konfirmasi->pesanan_id = $pesanan->id;
        // $konfirmasi->nomor_rekening = $request->nomor_rekening;
        // $konfirmasi->nama_account = $request->nama_account;
        // $konfirmasi->save();
        return view('ui/konfirmasi/index');
    }

    public function confrim(Request $request, $id)
    {
        $pesanan = Pesanan::where('id', $id)->first();
        $konfirmasi_pembayaran = Konfirmasi_pembayaran::where('pesanan_id', $pesanan->id)->get();
        // $konfirmasi = Konfirmasi_pembayaran::where('pesanan_id', $barang->id);

        $konfirmasi = new Konfirmasi_pembayaran;
        $konfirmasi->pesanan_id = $pesanan->id;
        $konfirmasi->nomor_rekening = $request->nomor_rekening;
        $konfirmasi->nama_account = $request->nama_account;
        $konfirmasi->save();

        return view('ui/konfirmasi/konfirmasi', compact('pesanan', 'konnfirmasi_pembayaran'));
    }
}
