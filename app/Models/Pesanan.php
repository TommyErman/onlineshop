<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pesanan extends Model
{

    protected $fillable = ['user_id', 'tanggal', 'status', 'kode', 'jumlah_harga'];
    protected $hidden = ['created_at', 'updated_at'];

    public function user()
    {
        //relasi dari model user ke pesanan
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    public function pesanan_detail()
    {
        //relasi dari model pesana ke pesanan_detail
        return $this->hasMany(PesananDetail::class, 'pesanan_id', 'id');
    }

    // public function Konfirmasi_pembayaran()
    // {
    //     //relasi dari model pesana ke pesanan_detail
    //     return $this->hasMany(Konfirmasi_pembayaran::class, 'pesanan_id', 'id');
    // }
}
