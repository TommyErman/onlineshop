<?php


use App\Http\Controllers\DashboardController;
use App\Http\Controllers\KonfirmasiController;
use App\Http\Controllers\BarangController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\BelanjaController;
use App\Http\Controllers\HistoryController;
use Illuminate\Support\Facades\App;

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('page/login');
});
// Route::get('/', [LoginController::class, 'showLoginForm']);
// Route::get('/register', [RegisterController::class, 'showLoginForm']);

Auth::routes();

route::group(['middleware' => ['auth', 'cekRole:admin,user']], function () {

    // Route::get('/home', [App\Http\Controllers\HomeController::class, 'index']);
    // route::get('home', HomeController::class, 'index');
    Route::get('/home', [HomeController::class, 'index'])->name('home');

    Route::get('profile', [ProfileController::class, 'index']);
    Route::post('profile', [ProfileController::class, 'update']);

    Route::get('history', [HistoryController::class, 'index']);
    Route::get('history/{id}', [HistoryController::class, 'detail']);




    Route::get('/pesan', [BelanjaController::class, 'index']);
    Route::get('/pesan/detail/{id}', [BelanjaController::class, 'detail']);
    Route::post('/pesan/detail/{id}', [BelanjaController::class, 'pesan']);
    Route::get('checkout', [BelanjaController::class, 'checkout']);
    Route::delete('checkout/{id}', [BelanjaController::class, 'delete']);
    Route::get('konfirmasi-check-out', [BelanjaController::class, 'konfirmasi']);

    // Route::get('/barang', [App\Http\Controllers\BarangController::class, 'index'])->name('barang');
    // Route::get('/barang/detailbarang', [App\Http\Controllers\BarangController::class, 'show'])->name('detailbarang');
    // Route::resource('barangs', BarangController::class);

    route::get('/konfirmasi-pembayaran', [KonfirmasiController::class, 'index']);
    route::post('/konfirmasi-pembayaran/{id}', [KonfirmasiController::class, 'confrim']);
});

route::group(['middleware' => ['auth', 'cekRole:admin']], function () {
    //#admin
    Route::resource('dashboard', DashboardController::class);
    Route::resource('barang', BarangController::class);
    Route::resource('user', UserController::class);
});
