<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use App\Models\Pesanan;
use App\Models\PesananDetail;
use Carbon\Carbon;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BelanjaController extends Controller
{
    public function index()
    {
        $barangs = Barang::paginate(20);
        return view('ui/pesanan/index', compact('barangs'));
    }

    public function detail($id)
    {
        $barang = Barang::where('id', $id)->first();
        return view('ui/pesanan/detail', compact('barang'));
    }

    public function pesan(Request $request, $id)
    {
        $barang = Barang::where('id', $id)->first();
        $tanggal = Carbon::now();

        //validasi apakah melebihi stok
        //(jika pesanan lebih dari stok maka tidak bisa memesan dan akan dikembalikan ke route pesanan)
        if ($request->jumlah_pesan > $barang->stok) {
            return redirect('pesan/detail' . $id);
        }

        //cek validasi user_id di pesanan join id yang ada di user
        $cek_pesanan = Pesanan::where('user_id', Auth::user()->id)->where('status', 0)->first();

        //simpan kedalam database

        if (empty($cek_pesanan)) {
            $pesanan = new Pesanan;
            $pesanan->user_id = Auth::user()->id;
            $pesanan->tanggal = $tanggal;
            $pesanan->status = 0;
            $pesanan->jumlah_harga = 0;
            $pesanan->kode = mt_rand(100, 999);
            $pesanan->save();
        }

        //simpan ke database detail_pesanan

        $pesanan_baru = Pesanan::where('user_id', Auth::user()->id)->where('status', 0)->first();

        //cek pesanan detail
        $cek_pesanan_detail = PesananDetail::where('barang_id', $barang->id)->where('pesanan_id', $pesanan_baru->id)->first();

        //(untuk jika pesanan sudah ada tidak usah membuat data pesanan baru )
        if (empty($cek_pesanan_detail)) {

            $pesanan_detail = new PesananDetail;
            $pesanan_detail->barang_id = $barang->id;
            $pesanan_detail->pesanan_id = $pesanan_baru->id;
            $pesanan_detail->jumlah = $request->jumlah_pesan;
            $pesanan_detail->jumlah_harga = $barang->harga * $request->jumlah_pesan;
            $pesanan_detail->save();
        } else {
            //jika pesanan sudah ada pesanan maka jika mau pesan pesanan baru maka tambahkan saja ke barang_id dan pesanan id user tersebut
            $pesanan_detail = PesananDetail::where('barang_id', $barang->id)->where('pesanan_id', $pesanan_baru->id)->first();

            $pesanan_detail->jumlah = $pesanan_detail->jumlah + $request->jumlah_pesan;


            //harga sekarang
            //(harga yg sudah di update dari harga tolal lama menjadi harga total yg baru)
            $harga_pesanan_detail_baru = $barang->harga * $request->jumlah_pesan;
            $pesanan_detail->jumlah_harga = $pesanan_detail->jumlah_harga + $harga_pesanan_detail_baru;
            $pesanan_detail->update();
        }

        //jumlah total

        $pesanan = Pesanan::where('user_id', Auth::user()->id)->where('status', 0)->first();
        $pesanan->jumlah_harga = $pesanan->jumlah_harga + $barang->harga * $request->jumlah_pesan;

        $pesanan->update();

        // Alert::success('Pesanan Sukses Masuk ke Kerajang', 'Success');

        // SweetAlert::message('Pesan Sudah Masuk Ke Keranjang', 'Success');

        return redirect('/checkout')->with('status', 'Pesan Sudah Masuk Ke Keranjang!!');
    }

    public function checkout()
    {

        $pesanan = Pesanan::where('user_id', Auth::user()->id)->where('status', 0)->first();
        $pesanan_details = [];
        if (!empty($pesanan)) {

            $pesanan_details = PesananDetail::where('pesanan_id', $pesanan->id)->get();
        }

        return view('ui/pesanan/checkout', compact('pesanan', 'pesanan_details'));
    }


    public function delete($id)
    {
        $pesanan_detail = PesananDetail::where('id', $id)->first();

        $pesanan = Pesanan::where('id', $pesanan_detail->pesanan_id)->first();
        $pesanan->jumlah_harga = $pesanan->jumlah_harga - $pesanan_detail->jumlah_harga;
        $pesanan->update();

        $pesanan_detail->delete();

        return redirect('/checkout')->with('status', 'Pesan Berhasil Di Hapus');
    }


    public function konfirmasi()
    {

        $pesanan = Pesanan::where('user_id', Auth::user()->id)->where('status', 0)->first();
        $pesanan_id = $pesanan->id;
        $pesanan->status = 1;
        $pesanan->update();

        $pesanan_details = PesananDetail::where('pesanan_id', $pesanan_id)->get();

        foreach ($pesanan_details as $pesanan_detail) {

            $barang = Barang::where('id', $pesanan_detail->barang_id)->first();
            $barang->stok = $barang->stok - $pesanan_detail->jumlah;
            $barang->update();
        }

        return redirect('/history')->with('status', 'Pesanan sukses di checkout');
    }
}
