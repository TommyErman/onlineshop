
<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
    integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css2?family=Quicksand&family=Viga&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="{{asset('style')}}/plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="{{asset('style')}}/assets/css/style.css">
  <title>ESTHETIC</title>
</head>

<body>
  <div class="container">
    <div class="row">
        <div class="col-md-12">
            <a href="{{ url('home') }}" class="btn btn-primary mt-3"><i class="fa fa-arrow-left"></i> Kembali</a>
        </div>
        <div class="col-md-12 mt-2">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('home') }}">detail</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Konfirmasi Pembayaran</li>
                </ol>
            </nav>
          </div>
         <div class="col-md-12">
            <div class="card">
              <div class="card-body">
                <h3><i class="fa fa-history"></i> Konfirmasi Pembayaran</h3>
               <form action="{{url('/konfirmasi-pembayaran/{id}')}}" method="post">
              @csrf
                <div class="col-md-6">
                  <div class="input-group mb-3">
                    <input type="nomor_rekening" name="nomor_rekening" id="nomor_rekening" class="form-control" placeholder="Masukan Nomor Rekening">
                      <div class="input-group-append">
                        <div class="input-group-text">
                          <span class="fas fa-envelope"></span>
                        </div>
                      </div>
                    </div>

                  <div class="input-group mb-3">
                     <input type="nama_account" name="nama_account" id="nama_account" class="form-control" placeholder="Nama Account">
                      <div class="input-group-append">
                        <div class="input-group-text">
                          <span class="fas fa-envelope"></span>
                        </div>
                      </div>
                    </div>

                    <form method="post" action="" >
                      @csrf
                          <button type="submit" class="btn btn-primary"></i> Kirim</button>
                    </form>

                </div>
               </form>
              </div>
            </div>
         </div>


    </div>
</div>
  



  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
    integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
    crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
    integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
    crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
    integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
    crossorigin="anonymous"></script>
    {{-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> --}}
</body>

</html>

