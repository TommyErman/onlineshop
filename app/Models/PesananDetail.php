<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PesananDetail extends Model
{
    protected $fillable = ['barang_id', 'pesanan_id', 'jumlah', 'jumlah_harga'];
    protected $hidden = ['created_at', 'updated_at'];

    public function barang()
    {
        //relasi dari barang ke pesanan_detail
        return $this->belongsTo(Barang::class, 'barang_id', 'id');
    }
    public function pesanan()
    {
        //relasi dari pesanan ke pesanan_detail
        return $this->belongsTo(Pesanan::class, 'pesanan_id', 'id');
    }
}
