
<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
    integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css2?family=Quicksand&family=Viga&display=swap" rel="stylesheet">

  <link rel="stylesheet" href="{{asset('style')}}/assets/css/style.css">
  <title>ESTHETIC</title>
</head>

<body class="profile">
  <!-- navbar -->

  <nav class="navbar navbar-expand-lg navbar-light">
    <div class="container">
      <a class="navbar-brand" href="#">ESTHETIC</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
        aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav ml-auto">
          <a class="nav-item nav-link active" href="{{route('home')}}">Profile<span class="sr-only">(current)</span></a>
          <a class="nav-item nav-link" href="{{url('/pesan')}}">Belanja</a>
          <a class="nav-item nav-link" href="kategori.html">Ketegori</a>
          <a class="nav-item nav-link" href="#">Transaksi</a>
          <li class="nav-item dropdown">
            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                {{ Auth::user()->name }} <span class="caret"></span>
            </a>

            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="{{ url('profile') }}">
                    Profile
                </a>

                <a class="dropdown-item" href="{{ url('history') }}">
                    Riwayat Pemesanan
                </a>

                <a class="dropdown-item" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
          </li>
          @if (auth()->user()->role =='admin')
          <a class="nav-item btn btn-primary tombol" href="{{route('barang')}}">Admin</a>
          @endif
        </div>
      </div>
    </div>
  </nav>
  <!-- AKHIR NAV -->



  <!-- JUMBOTRON -->
  <div class="jumbotron jumbotron-fluid">
    <div class="container">
      <h1 class="display-4"><span>Esthetic</span> High Quality <span>Perfume</span> </h1>
    
    </div>
  </div>
  <!-- AKHIR -->


  <!-- container -->

  <div class="container">

    <!-- info panel -->
    <div class="row justify-content-center">
      <div class="col-10">
        <div class="row info-panel">
          <div class="col-lg">
            <img src="{{asset('style')}}/assets/img/store.png" alt="store" class="float-left">
            <h4>Open Store</h4>
            <p> Senin - Sabtu<br>10:00 - 22:00 <br>minggu <br> 11:00 - 22:00</p>
          </div>
          <div class="col-lg">
            <img src="{{asset('style')}}/assets/img/lokasi.png" alt="lokasi" class="float-left">
            <h4>Address</h4>
            <p>Jl. Tegallega Ciheulet Pakuan Rt.03/06 (samping alfa) <br>Kota Bogor. </p>
          </div>
          <div class="col-lg">
            <img src="{{asset('style')}}/assets/img/sosial.png" alt="sosial" class="float-left">
            <h4>Social</h4>
            <img src="{{asset('style')}}/assets/img/instagram.png" alt="instagram" class="minimalis"><span>Esthetic_Perfume</span>
            <br>
            <img src="{{asset('style')}}/assets/img/fb.png" alt="instagram" class="minimalis "><span>Esthetic_Perfume</span>
            <br>
            <img src="{{asset('style')}}/assets/img/wa.png" alt="instagram" class="minimalis "><span>087749794609</span>

          </div>

        </div>
      </div>
    </div>

    <div class="row work">
      <div class="col-lg-6">
        <img src="{{asset('style')}}/assets/img/store2.png" alt="esthetic" class="img-fluid">


      </div>

      <div class="col-lg-6">
        <h3>About</h3>
        <p>Esthetic Perfume adalah toko perfume yang terletak di Kota Bogor lebih tepatnya di Jl. tegallega dekat
          univ.
          pakuan. kami selalu menjaga kualitas terbaik dengan harga terjangkau agar para konsumen selalu mendapatkan
          parfum yang terbaik.</p>
        <a href="" class="btn btn-primary tombol">Belanja Sekarang</a>
      </div>
    </div>
    <!-- akhir container -->
  </div>

  <!-- partner -->
  <section class="partner" id="partner">
    <div class="row">
      <div class="col-12">
        <h3 class="text-center">Partner</h3>
      </div>
    </div>
    <div class="container">
      <div class="row client ">
        <div class="col-lg ">
          <img src="{{asset('style')}}/assets/img/zara.png" alt="" class="img-fluid">
        </div>
        <div class="col-lg">
          <img src="{{asset('style')}}/assets/img/ck2.png" alt="" class="img-fluid">
        </div>
        <div class="col-lg">
          <img src="{{asset('style')}}/assets/img/hugo.jpg" alt="" class="img-fluid">
        </div>
        <div class="col-lg">
          <img src="{{asset('style')}}/assets/img/aigner.png" alt="" class="img-fluid">
        </div>
      </div>
    </div>
  </section>
  <!-- akhir patner -->

  <section class="kategori" id="kategori">
    <div class="container">
      <div class="row">
        <div class="col-12 text-center">
          <h2>Kategori</h2>
        </div>
      </div>
      <div class="row">
        <div class="col-lg">
          <div class="card">
            <a href="">
              <img src="{{asset('style')}}/assets/img/botol/1.jpeg" alt="1" class="img-fluid">
              <div class="card-body">
                <h5 class="card-title">Men</h5>
            </a>
          </div>
        </div>
      </div>
      <div class="col-lg">
        <div class="card">
          <a href="">
            <img src="{{asset('style')}}/assets/img/botol/2.jpeg" alt="2" class="img-fluid">
            <div class="card-body">
              <h5 class="card-title">Women</h5>
          </a>
        </div>
      </div>
    </div>
    <div class="col-lg">
      <div class="card">
        <a href="">
          <img src="{{asset('style')}}/assets/img/botol/3.jpeg" alt="3" class="img-fluid">
          <div class="card-body">
            <h5 class="card-title">Unisex</h5>
          </div>
        </a>
      </div>
    </div>
    </div>
  </section>



  <section class="kontak" id="kontak">
    <div class="container">
      <div class="row">
        <div class="col text-center">
          <h2>Kontak</h2>
        </div>
      </div>


      <div class="row justify-content-center">
        <div class="col-lg-4">
          <div class="card bg-primary text-white mb-4 text-center">
            <div class="card-body">
              <h5 class="card-title">Contact Me</h5>
              <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's
                content.</p>
            </div>
          </div>
          <ul class="list-group">
            <li class="list-group-item active">Location</li>
            <li class="list-group-item">Dapibus ac facilisis in</li>
            <li class="list-group-item">Morbi leo risus</li>
            <li class="list-group-item">Porta ac consectetur ac</li>
            <li class="list-group-item">Vestibulum at eros</li>
          </ul>
        </div>



        <div class="col-lg-6">
          <form>
            <div class="form-group">
              <label for="nama">Nama</label>
              <input type="text" class="form-control" id="nama">
            </div>
            <div class="form-group">
              <label for="email">Email</label>
              <input type="text" class="form-control" id="email">
            </div>
            <div class="form-group">
              <label for="phone">Phone Number</label>
              <input type="text" class="form-control" id="phone">
            </div>
            <div class="form-group">
              <label for="message">Message</label>
              <textarea class="form-control" id="message" rows="3"></textarea>
            </div>
            <div class="form-group">
              <button type="button" class="btn btn-primary">Send Message</button>
            </div>
          </form>
        </div>
      </div>
    </div>

  </section>























  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
    integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
    crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
    integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
    crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
    integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
    crossorigin="anonymous"></script>
</body>

</html>