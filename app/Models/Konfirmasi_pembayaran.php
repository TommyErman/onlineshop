<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Konfirmasi_pembayaran extends Model
{

    protected $fillable = ['pesanan_id', 'nomor_rekening', 'nama_account'];
    public function pesanan()
    {
        //relasi dari pesanan ke pesanan_detail
        return $this->belongsTo(Pesanan::class, 'pesanan_id', 'id');
    }
}
