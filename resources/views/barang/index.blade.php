@extends('layout/main')
@section('title','Data Barang')
    

@section('breadcrumbs')
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">Data Barang</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active">Data Barang</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
@endsection


@section('content')
<a href="{{url('barang/create')}}" class="btn btn-primary mb-3"> 
  <i class="fa fa-plus"></i> Tambah Data</a>

  @if (session('status'))
          <div class="alert alert-success">
              {{ session('status') }}
          </div>
  @endif

     <!-- Main content -->
     <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg">
          <table class="table table-hover">
            <thead>
              <tr>
                <th scope="col">No</th>
                <th scope="col">Nama Barang</th>
                <th scope="col">Harga</th>
                <th scope="col">Stok</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            @foreach ($barangs as $item)
            <tbody>
              <tr>
                <th scope="row">{{$loop->iteration}}</th>
                <td>{{$item->nama_barang}}</td>
                <td>{{$item->harga}}</td>
                <td>{{$item->stok}}</td>
                <td>
                <a href="{{url('barang/' .$item->id)}}" class="btn btn-sm btn-primary">detail</a>
                  <a href="{{url('barang/' .$item->id. '/edit')}}" class="btn btn-sm btn-warning">edit</a>
                  <form action="{{url('barang/' .$item->id)}}" method="POST" onsubmit="return confirm('yakin hapus data ?')" class="d-inline">
                    @method('delete')
                    @csrf
                    <button class="btn btn-sm btn-danger">hapus</button>
                  </form>
                </td>
              </tr>
            </tbody>
            @endforeach
          </table>
        </div>
      </div>
    </div>
  <!-- /.content -->
@endsection





   

 
