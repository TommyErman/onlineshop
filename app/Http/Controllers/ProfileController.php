<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function index()
    {
        $user = User::where('id', Auth::user()->id)->first();
        return view('ui/profile/index', compact('user'));
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'password'  => 'confirmed',
        ]);

        $user = User::where('id', Auth::user()->id)->first();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->nomor_telepon = $request->nomor_telepon;
        $user->alamat = $request->alamat;
        if (!empty($request->password)) {
            $user->password = bcrypt($request->password);
        }

        $user->update();

        // Alert::success('User Sukses diupdate', 'Success');
        return redirect('profile')->with('status', 'profile berhasil di update');
    }
}
