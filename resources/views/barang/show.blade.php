@extends('layout/main')
@section('title','Detail Barang')
    

@section('breadcrumbs')
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">Detail</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Data Barang</a></li>
          <li class="breadcrumb-item active">Detail Barang</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->

@endsection

@section('content')

     <!-- Main content -->
     <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg">

          <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <img src="{{ url('images') }}/{{ $barang->gambar }}" class="rounded mx-auto d-block" width="100%" alt=""> 
                    </div>
                    <div class="col-md-6 mt-5">
                        <h2>{{ $barang->nama_barang }}</h2>
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td>Harga</td>
                                    <td>:</td>
                                    <td>Rp. {{ number_format($barang->harga) }}</td>
                                </tr>
                                <tr>
                                    <td>Stok</td>
                                    <td>:</td>
                                    <td>{{ number_format($barang->stok) }}</td>
                                </tr>
                                <tr>
                                    <td>Keterangan</td>
                                    <td>:</td>
                                    <td>{{ $barang->keterangan }}</td>
                                </tr>
                                <tr>
                                  <td>
                                          <a href="{{url('barang')}}" class="btn btn-primary mt-2">Kembali</a>
                                     
                                  </td>
                              </tr>
                             
                               
                               
                                
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
             <!-- Main content -->
     {{-- <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-6">
            <table class="table table-bordered">
              <tbody>
                <tr>
                  <th>Nama Barang</th>
                 <td>{{$barang->nama_barang}}</td>
                </tr>

                <tr>
                  <th>Harga</th>
                 <td>{{$barang->harga}}</td>
                </tr>

                <tr>
                  <th>Stok</th>
                 <td>{{$barang->stok}}</td>
                </tr>

                <tr>
                  <th>Keterangan</th>
                 <td>{{$barang->keterangan}}</td>
                </tr>
                
                  <tr>
                    <th>Gambar</th>
                   <td>{{$barang->gambar}}</td>
                  </tr>
              
                <tr>
                  <th>Tanggal Daftar</th>
                 <td>{{$barang->craate_at}}</td>
                </tr>
                
              </tbody>
            </div>  
            <a href="{{url('barang')}}" class="btn btn-primary mb-3">Back</a>
          </div>
        </div> --}}
        </div>
      </div>
    </div>
  <!-- /.content -->
@endsection





   

 
