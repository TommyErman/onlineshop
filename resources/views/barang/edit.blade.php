@extends('layout/main')
@section('title','Edit Data Barang')
    

@section('breadcrumbs')
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">Edit Data Barang</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Data Barang</a></li>
          <li class="breadcrumb-item active">Edit</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->

@endsection

@section('content')
     <!-- Main content -->
     <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-6">
          <form action="{{url('barang/' .$barang->id)}}" method="POST">
            @method('PUT')
            @csrf
            <div class="form-group">
              <label for="name">Nama Barang</label>
              <input type="name" class="form-control @error('nama_barang') is-invalid @enderror" id="nama_barang" name="nama_barang" placeholder="Nama Barang" value="{{ old('nama_barang' ,$barang->nama_barang) }}">
              @error('nama_barang')
              <div class="invalid-feedback">{{ $message }}</div>
              @enderror
            </div>

            <div class="form-group">
              <label for="name">Harga</label>
              <input type="harga" class="form-control @error('harga') is-invalid @enderror" id="harga" name="harga" placeholder="Harga" value="{{ old('harga' ,$barang->harga) }}">
              @error('harga')
              <div class="invalid-feedback">{{ $message }}</div>
              @enderror
            </div>

            <div class="form-group">
              <label for="name">Stok</label>
              <input type="stok" class="form-control @error('stok') is-invalid @enderror" id="stok" name="stok" placeholder="Stok" value="{{old('stok' ,$barang->stok) }}" >
              
  
              @error('stok')
                  <div class="invalid-feedback">{{ $message }}</div>
              @enderror
            </div>

            <div class="form-group">
              <label for="gambar">Gambar</label>
              <input type="file" class="form-control @error('gambar') is-invalid @enderror" id="gambar" name="gambar" value="{{old('gambar'.$barang->gambar)}}">
              @error('gambar')
              <div class="invalid-feedback">{{ $message }}</div>
              @enderror

            </div>
            <div class="form-group">
              <label for="keterangan">Keterangan</label>
              <input type="keterangan" class="form-control @error('keterangan') is-invalid @enderror" id="keterangan" name="keterangan" placeholder="Keterangan" value="{{old('keterangan' ,$barang->keterangan) }}">

              @error('keterangan')
              <div class="invalid-feedback">{{ $message }}</div>
              @enderror

            </div>
            
            <button type="submit" class="btn btn-primary">Submit</button>
          </form>
        </div>
      </div>
    </div>
  <!-- /.content -->
@endsection





   

 
